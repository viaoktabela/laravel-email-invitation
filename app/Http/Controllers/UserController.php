<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use DataTables;

class UserController extends Controller
{
    public function index()
    {
        if(!Session::get('login'))
        {
            return redirect('login')->with('alert','You have to login first!');
        }
        else
        {
            $user['count'] = User::get()->count();
            return view('main', $user);
        }
    }

    function check_auth(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        $data = User::where('email', $email)->first();
        if($data)
        {
            if(Hash::check($password, $data->password))
            {
                $request->session()->put('id', $data->id);
                $request->session()->put('name', $data->name);
                $request->session()->put('email', $data->email);
                $request->session()->put('login', TRUE);
                $request->session()->put('role', $data->role);

                return redirect('/');
            }
            else
            {
                return redirect('/login')->with('alert', 'Your email or password is not correct!');
            }
        }
        else
        {
            return redirect('/login')->with('alert', 'Cannot find the account with the credentials');
        }
    }

    function signout()
    {
        Session::flush();
        return redirect('/login')->with('alert',"You've logout!");
    }

    function view_user()
    {
        // $data = User::where('role', '2')->get();
        return view('user');
    }

    function user_list()
    {
        $users = User::where('role', '2');
        return DataTables::of($users)
            ->addColumn('action', function ($user) {
                return '<a href = "edit-user/'. $user->id .'" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
                <a href= "delete-user/'. $user->id .'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>';
            })
            ->toJson();
    }

    function edit_user($id)
    {
        $user = User::findOrFail($id);
        return view('user', compact('user'));
    }

    function add_user(Request $request)
    {
        $user = User::where('email',$request->email);
        if(!isset($user->name))
        {
            $user = array(
                'name' => $request->name,
                'email'=> $request->email,
                'password' => $request->email,
                'role'  => '2'
            );
            $user = new User;
            $user->name = $request->name;
            $user->email= $request->email;
            $user->password = $request->email;
            $user->role  = '2';

            $user->save();

            $to_name = $request->name;
            $to_email = $request->email;

            $data = array("name"=>"$to_name", "body" => url('user/update-detail/'.$user->id));
            Mail::send("email.mail", $data, function($message) use ($to_name, $to_email)
            {
                $message->from('tjungyuni@gmail.com', 'Laravel Invitation')
                        ->to($to_email, $to_name)
                        ->subject("Invitation");
            });

            return redirect('/user');
        }
        else
        {
            return redirect('/user')->with('alert', 'Email has been used!');
        }
    }

    function update_user(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->email = $request->email;
        if(!empty($request->password))
            $user->password = bcrypt($request->password);
        $user->save();

        if(Session::get('login')){
            if(Session::get('role') == '1')
            {
                return redirect('/user');
            }
            else return redirect('/');
        }
        else
            return redirect('/login');
    }

    function delete_user($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('/user');
    }

    function update_profile()
    {
        $id = Session::get('id');
        $user = User::findOrFail($id);
        return view('user', compact('user'));
    }

    function update_detail($id)
    {
        $user = User::findOrFail($id);
        return view('verification', compact('user'));
    }


}
