@extends('master')
@section('title', 'User')
@section('content')
    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content container-fluid">
        <div class="col-md-12">
            @if (isset($user))
            <div class="box box-primary">
                    <div class="box-header">
                        <h1 class="box-title">Update Data User</h1>
                    </div>
                    <div class="box-body">
                        <form action="{{ route('user.update', ['id' => $user->id ])}}" method="post" class = "col-md-6">
                            @csrf
                            <div class="form-group">
                                <label for="" class="control-label">Name</label>
                                <input type="text" name="name" id="name" class="form-control" value="{{ $user->name }}">
                            </div>
                            <div class="form-group">
                                <label for="" class="control-label">Email</label>
                                <input type="email" name="email" id="email" class="form-control" value="{{ $user->email }}">
                            </div>
                            <div class="form-group">
                                    <label for="" class="control-label">Password</label>
                                    <input type="password" name="password" id="password" class="form-control">
                                </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            @else
                <div class="box box-primary">
                    <div class="box-header">
                        <h1 class="box-title">List Users</h1>
                        <div class="body-tools">
                            <button class="btn btn-primary pull-right" data-target = "#modal_invite" data-toggle = "modal">Invite User</button>
                        </div>
                    </div>
                    <div class="box-body">
                        @if(Session::has('alert'))
                        <div class="col-md-4">
                            <div class="alert alert-danger">
                                <div>{{Session::get('alert')}}</div>
                            </div>
                        </div>
                        @endif
                        <table class = "table table-bordered" id = "table_user">
                            <thead>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Action</th>
                            </thead>
                        </table>
                    </div>
                </div>

            @endif
        </div>

    </section>

      <div class="modal modal-default fade" id="modal_invite">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Invite User</h4>
            </div>
            <form action="{{ url('/add_user') }}" method="post">
                {{ csrf_field() }}

            <div class="modal-body">
              <div class="form-group">
                  <label for="" class="control-label">Email</label>
                  <input type="email" name="email" id="email" class="form-control">
              </div>
              <div class="form-group">
                    <label for="" class="control-label">Name</label>
                    <input type="text" name="name" id="name" class="form-control">
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-outline-danger pull-left" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@push('scripts')

<script>
        $(document).ready(function(){
          $('#table_user').DataTable({
              processing: true,
              serverSide: true,
              ajax: '{!! route('user.user-list') !!}',
              columns: [
                       { data: 'name', name: 'name' },
                       { data: 'email', name: 'email' },
                       { data: 'action', name: 'action'}
                    ],
              paging: false,
              destroy: true
          });
      });
      </script>
@endpush